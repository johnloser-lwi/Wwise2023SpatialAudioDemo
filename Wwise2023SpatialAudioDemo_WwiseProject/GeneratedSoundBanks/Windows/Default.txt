Event	ID	Name			Wwise Object Path	Notes
	1602358412	Play_Footstep			\Default Work Unit\Play_Footstep	
	2748311587	Play_RoomTone			\Default Work Unit\Play_RoomTone	
	2932040671	Play_Music			\Default Work Unit\Play_Music	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	51613554	RoomTone	E:\Wwise2023SpatialAudioDemo\Wwise2023SpatialAudioDemo_WwiseProject\.cache\Windows\SFX\Ambience, Room Tone, Small Room, Heating Pipe, Resonance, Tonal Noise (LCR) SND36939_02EAB89D.wem		\Actor-Mixer Hierarchy\Default Work Unit\RoomTone		3309564
	385984820	Footstep	E:\Wwise2023SpatialAudioDemo\Wwise2023SpatialAudioDemo_WwiseProject\.cache\Windows\SFX\Footsteps, Human, Asphalt, Male, Walk SND11513 7_818F3B66.wem		\Actor-Mixer Hierarchy\Default Work Unit\Footstep		35616
	508902323	Music	E:\Wwise2023SpatialAudioDemo\Wwise2023SpatialAudioDemo_WwiseProject\.cache\Windows\SFX\Musical, Song & Phrase, Deep Techno, Arp Chord SND84895_818F3B66.wem		\Actor-Mixer Hierarchy\Default Work Unit\Music		14649008

